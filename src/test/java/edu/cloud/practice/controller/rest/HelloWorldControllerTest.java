package edu.cloud.practice.controller.rest;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class HelloWorldControllerTest {

    private HelloWorldController testInstance = new HelloWorldController();

    @Test
    public void shouldReturnHelloWorldString() {
        assertThat(testInstance.hello()).isEqualTo("hello world");
    }

}