package edu.cloud.practice.controller.rest;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import edu.cloud.practice.jpa.entities.Category;
import edu.cloud.practice.jpa.entities.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.hateoas.server.core.TypeReferences;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;
import java.net.URI;
import java.util.Set;

import static java.util.Comparator.comparing;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.hateoas.client.Hop.rel;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CategoriesControllerTest {

    @Autowired
    private ObjectMapper om;

    @Autowired
    private TestRestTemplate rest;

    @Autowired
    private DataSource dataSource;

    @Value("${spring.data.rest.base-path}")
    private String dataRestBasePath;

    @Value("${spring.datasource.schema}")
    private Resource schema;

    @Value("${spring.datasource.data}")
    private Resource data;

    private Product expectedProduct_1 = Product.builder().name("palmolive").price(32.21).build();
    private Product expectedProduct_2 = Product.builder().name("timotey").price(12.34).build();

    private Category expectedCategory_1 = Category.builder().name("verhnyaya polochka").build();
    private Category expectedCategory_2 = Category.builder().name("nijnyaya polochka").build();

    private Category newCategory = Category.builder().name("new category").build();

    private TypeReferences.CollectionModelType<Category> categoriesType = new TypeReferences.CollectionModelType<>() {};
    private TypeReferences.CollectionModelType<Product> productsType = new TypeReferences.CollectionModelType<>() {};

    @BeforeEach
    public void setUp() {
        new ResourceDatabasePopulator(schema, data).execute(dataSource);
    }

    @Test
    public void shouldReturnAllCategories() {
        CollectionModel<Category> response = prepareTraverson().follow(rel("categories")).toObject(categoriesType);

        assertThat(response)
                .usingElementComparator(comparing(Category::getName))
                .containsExactlyInAnyOrder(expectedCategory_1, expectedCategory_2);
    }

    @Test
    public void shouldReturnSpecifiedCategory() {
        Category actualCategory = rest.getForObject(dataRestBasePath + "/categories/1", Category.class);

        assertThat(actualCategory).usingComparator(comparing(Category::getName)).isEqualTo(expectedCategory_1);
    }

    @Test
    public void shouldReturnAllCategoryProducts() {
        CollectionModel<Product> response = prepareTraverson("categories", "1")
                .follow("$._links.products.href")
                .toObject(productsType);

        assertThat(response)
                .usingElementComparator(comparing(Product::getName).thenComparing(Product::getPrice))
                .containsExactlyInAnyOrder(expectedProduct_1, expectedProduct_2);
    }

    @Test
    public void shouldReturnAllCategoriesSortedByNameAsc() {
        CollectionModel<Category> response = prepareTraverson()
                .follow(rel("categories")
                                .withParameter("sort", "name,asc"))
                .toObject(categoriesType);

        assertThat(response)
                .usingElementComparator(comparing(Category::getName))
                .containsExactly(expectedCategory_2, expectedCategory_1);
    }

    @Test
    public void shouldReturnAllCategoriesSortedByNameDesc() {
        CollectionModel<Category> response = prepareTraverson()
                .follow(rel("categories")
                                .withParameter("sort", "name,desc"))
                .toObject(categoriesType);

        assertThat(response)
                .usingElementComparator(comparing(Category::getName))
                .containsExactly(expectedCategory_1, expectedCategory_2);
    }

    @Test
    public void shouldReturnAllCategoriesSortedByProductCountAsc() {
        CollectionModel<Category> response = prepareTraverson()
                .follow(rel("categories"))
                .follow("$._links.search.href")
                .follow(rel("findAllByProductsCountAsc"))
                .toObject(categoriesType);

        assertThat(response)
                .usingElementComparator(comparing(Category::getName))
                .containsExactly(expectedCategory_1, expectedCategory_2);
    }

    @Test
    public void shouldReturnAllCategoriesSortedByProductCountDesc() {
        CollectionModel<Category> response = prepareTraverson()
                .follow(rel("categories"))
                .follow("$._links.search.href")
                .follow(rel("findAllByProductsCountDesc"))
                .toObject(categoriesType);

        assertThat(response)
                .usingElementComparator(comparing(Category::getName))
                .containsExactly(expectedCategory_2, expectedCategory_1);
    }

    @Test
    public void shouldCreateCategory() {
        String createdEntityUri = rest.postForObject(dataRestBasePath + "/categories", newCategory, JsonNode.class)
                                      .get("_links").get("self").get("href").asText();

        Category createdCategory = rest.getForObject(createdEntityUri, Category.class);

        assertThat(createdCategory)
                .usingComparator(comparing(Category::getName))
                .isEqualTo(newCategory);
    }

    @Test
    public void shouldCreateCategoryWithProducts() {
        newCategory.setProducts(Set.of(expectedProduct_2));
        ObjectNode newCategoryJson = om.convertValue(newCategory, ObjectNode.class);
        newCategoryJson.putArray("products").add(rest.getRootUri() + dataRestBasePath + "/products/2");
        String createdEntityUri = rest.postForObject(dataRestBasePath + "/categories", newCategoryJson,
                                                     JsonNode.class).get("_links").get("self").get("href").asText();

        CollectionModel<Product> createdCategoryProducts = prepareTraverson(createdEntityUri)
                .follow("$._links.products.href")
                .toObject(productsType);

        assertThat(createdCategoryProducts)
                .usingElementComparator(comparing(Product::getName).thenComparing(Product::getPrice))
                .containsExactly(expectedProduct_2);
    }

    @Test
    public void shouldDeleteSpecifiedCategory() {
        rest.delete(dataRestBasePath + "/categories/1");

        ResponseEntity<String> response = rest.getForEntity(dataRestBasePath + "/categories/1", String.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void shouldUpdateCategory() {
        expectedCategory_1.setName("updated name");
        rest.put(dataRestBasePath + "/categories/1", expectedCategory_1);

        Category updatedCategory = rest.getForObject(dataRestBasePath + "/categories/1", Category.class);

        assertThat(updatedCategory).usingComparator(comparing(Category::getName)).isEqualTo(expectedCategory_1);
    }

    private Traverson prepareTraverson(String... rel) {
        URI uri = URI
                .create(rest.getRootUri())
                .resolve(dataRestBasePath + "/")
                .resolve(String.join("/", rel));
        return new Traverson(uri, MediaTypes.HAL_JSON);
    }

}
