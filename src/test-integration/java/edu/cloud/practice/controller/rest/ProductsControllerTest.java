package edu.cloud.practice.controller.rest;

import com.fasterxml.jackson.databind.JsonNode;
import edu.cloud.practice.jpa.entities.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Traverson;
import org.springframework.hateoas.server.core.TypeReferences;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import javax.sql.DataSource;
import java.net.URI;
import java.util.Comparator;

import static java.util.Comparator.comparing;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.hateoas.client.Hop.rel;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductsControllerTest {

    @Autowired
    private TestRestTemplate rest;

    @Autowired
    private DataSource dataSource;

    @Value("${spring.data.rest.base-path}")
    private String dataRestBasePath;

    @Value("${spring.datasource.schema}")
    private org.springframework.core.io.Resource schema;

    @Value("${spring.datasource.data}")
    private Resource data;

    private Product expectedProduct_1 = Product.builder().name("palmolive").price(32.21).build();
    private Product expectedProduct_2 = Product.builder().name("timotey").price(12.34).build();
    private Product expectedProduct_3 = Product.builder().name("pantine").price(2.54).build();
    private Product expectedProduct_4 = Product.builder().name("schauma").price(5.45).build();
    private Product newProduct = Product.builder().name("new product").price(9.99).build();

    private TypeReferences.CollectionModelType<Product> productsType = new TypeReferences.CollectionModelType<>() {};

    @BeforeEach
    public void setUp() {
        new ResourceDatabasePopulator(schema, data).execute(dataSource);
    }

    @Test
    public void shouldReturnAllProducts() {
        CollectionModel<Product> response = prepareTraverson().follow(rel("products")).toObject(productsType);

        assertThat(response)
                .usingElementComparator(Comparator.comparing(Product::getName).thenComparing(Product::getPrice))
                .containsExactlyInAnyOrder(expectedProduct_1, expectedProduct_2, expectedProduct_3, expectedProduct_4);
    }

    @Test
    public void shouldReturnSpecifiedProduct() {
        Product response = rest.getForObject(dataRestBasePath + "/products/1", Product.class);

        assertThat(response)
                .usingComparator(comparing(Product::getName).thenComparing(Product::getPrice))
                .isEqualTo(expectedProduct_1);
    }

    @Test
    public void shouldReturnProductsWithinPriceRange() {
        CollectionModel<Product> response = prepareTraverson()
                .follow(rel("products"))
                .follow("$._links.search.href")
                .follow(rel("findAllByPriceBetween")
                                .withParameter("lowerBoundary", "5.45")
                                .withParameter("upperBoundary", "12.34"))
                .toObject(productsType);

        assertThat(response)
                .usingElementComparator(Comparator.comparing(Product::getName).thenComparing(Product::getPrice))
                .containsExactlyInAnyOrder(expectedProduct_2, expectedProduct_4);
    }

    @Test
    public void shouldReturnProductsInCategoryWithinPriceRange() {
        CollectionModel<Product> response = prepareTraverson()
                .follow(rel("products"))
                .follow("$._links.search.href")
                .follow(rel("findAllByCategoryIdAndPriceBetween")
                                .withParameter("categoryId", "1")
                                .withParameter("lowerBoundary", "5.45")
                                .withParameter("upperBoundary", "12.34"))
                .toObject(productsType);

        assertThat(response)
                .usingElementComparator(Comparator.comparing(Product::getName).thenComparing(Product::getPrice))
                .containsExactly(expectedProduct_2);
    }

    @Test
    public void shouldReturnProductsWithNameContaining() {
        CollectionModel<Product> response = prepareTraverson()
                .follow(rel("products"))
                .follow("$._links.search.href")
                .follow(rel("findAllByNameContaining")
                                .withParameter("searchTerm", "pa"))
                .toObject(productsType);

        assertThat(response)
                .usingElementComparator(Comparator.comparing(Product::getName).thenComparing(Product::getPrice))
                .containsExactlyInAnyOrder(expectedProduct_1, expectedProduct_3);
    }

    @Test
    public void shouldReturnProductsInCategoryWithNameContaining() {
        CollectionModel<Product> response = prepareTraverson()
                .follow(rel("products"))
                .follow("$._links.search.href")
                .follow(rel("findAllByCategoryIdAndNameContaining")
                                .withParameter("categoryId", "1")
                                .withParameter("searchTerm", "pa"))
                .toObject(productsType);

        assertThat(response)
                .usingElementComparator(Comparator.comparing(Product::getName).thenComparing(Product::getPrice))
                .containsExactly(expectedProduct_1);
    }

    @Test
    public void shouldReturnAllProductsSortedByNameAsc() {
        CollectionModel<Product> response = prepareTraverson()
                .follow(rel("products")
                                .withParameter("sort", "name,asc"))
                .toObject(productsType);

        assertThat(response)
                .usingElementComparator(comparing(Product::getName))
                .containsExactly(expectedProduct_1, expectedProduct_3, expectedProduct_4, expectedProduct_2);
    }

    @Test
    public void shouldReturnAllProductsSortedByNameDesc() {
        CollectionModel<Product> response = prepareTraverson()
                .follow(rel("products")
                                .withParameter("sort", "name,desc"))
                .toObject(productsType);

        assertThat(response)
                .usingElementComparator(comparing(Product::getName))
                .containsExactly(expectedProduct_2, expectedProduct_4, expectedProduct_3, expectedProduct_1);
    }

    @Test
    public void shouldReturnAllProductsSortedByPriceAsc() {
        CollectionModel<Product> response = prepareTraverson()
                .follow(rel("products")
                                .withParameter("sort", "price,asc"))
                .toObject(productsType);

        assertThat(response)
                .usingElementComparator(comparing(Product::getPrice))
                .containsExactly(expectedProduct_3, expectedProduct_4, expectedProduct_2, expectedProduct_1);
    }

    @Test
    public void shouldReturnAllProductsSortedByPriceDesc() {
        CollectionModel<Product> response = prepareTraverson()
                .follow(rel("products")
                                .withParameter("sort", "price,desc"))
                .toObject(productsType);

        assertThat(response)
                .usingElementComparator(comparing(Product::getPrice))
                .containsExactly(expectedProduct_1, expectedProduct_2, expectedProduct_4, expectedProduct_3);
    }


    @Test
    public void shouldCreateProduct() {
        String createdEntityUri = rest.postForObject(dataRestBasePath + "/products", newProduct, JsonNode.class)
                                      .get("_links").get("self").get("href").asText();

        Product createdCategory = rest.getForObject(createdEntityUri, Product.class);

        assertThat(createdCategory)
                .usingComparator(comparing(Product::getName).thenComparing(Product::getPrice))
                .isEqualTo(newProduct);
    }

    @Test
    public void shouldDeleteSpecifiedProduct() {
        rest.delete(dataRestBasePath + "/products/1");

        ResponseEntity<String> response = rest.getForEntity(dataRestBasePath + "/products/1", String.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void shouldUpdateProduct() {
        expectedProduct_1.setName("updated name");
        expectedProduct_1.setPrice(66.6);
        rest.put(dataRestBasePath + "/products/1", expectedProduct_1);

        Product updatedProduct = rest.getForObject(dataRestBasePath + "/products/1", Product.class);

        assertThat(updatedProduct)
                .usingComparator(comparing(Product::getName).thenComparing(Product::getPrice))
                .isEqualTo(expectedProduct_1);

    }

    private Traverson prepareTraverson(String... rel) {
        URI uri = URI
                .create(rest.getRootUri())
                .resolve(dataRestBasePath + "/")
                .resolve(String.join("/", rel));
        return new Traverson(uri, MediaTypes.HAL_JSON);
    }

}
