package edu.cloud.practice.controller.rest;

import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/")
public class HelloWorldController {

    @Resource
    private Environment env;

    @GetMapping
    public String hello() {
        return "hello world";
    }

    @GetMapping("/test-message")
    public String getPropertyMessage() {
        return env.getProperty("test.message");
    }

}
