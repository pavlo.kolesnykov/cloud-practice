package edu.cloud.practice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource("classpath:message.properties")
public class CloudPracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudPracticeApplication.class, args);
	}

}
