package edu.cloud.practice.jpa.repositories;

import edu.cloud.practice.jpa.entities.Category;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Set;

@RepositoryRestResource
public interface CategoryRepository extends PagingAndSortingRepository<Category, Long> {

    @Query("Select cat from Category cat order by size(cat.products) asc")
    Set<Category> findAllByProductsCountAsc();

    @Query("Select cat from Category cat order by size(cat.products) desc")
    Set<Category> findAllByProductsCountDesc();
}
