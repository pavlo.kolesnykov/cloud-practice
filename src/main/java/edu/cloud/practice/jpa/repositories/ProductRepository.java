package edu.cloud.practice.jpa.repositories;

import edu.cloud.practice.jpa.entities.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface ProductRepository extends PagingAndSortingRepository<Product, Long> {
    Set<Product> findAllByPriceBetween(double lowerBoundary, double upperBoundary);

    @Query("select p from Category c join c.products p where c.id=?1 and p.price between ?2 and ?3")
    Set<Product> findAllByCategoryIdAndPriceBetween(Long categoryId, double lowerBoundary, double upperBoundary);

    Set<Product> findAllByNameContaining(String searchTerm);

    @Query("select p from Category c join c.products p where c.id=?1 and p.name like %?2%")
    Set<Product> findAllByCategoryIdAndNameContaining(Long categoryId, String searchTerm);

}
