set foreign_key_checks=0;

INSERT INTO categories VALUES (1, 'verhnyaya polochka');
INSERT INTO categories VALUES (2, 'nijnyaya polochka');

INSERT INTO products (id, name, price) VALUES (1, 'palmolive', 32.21);
INSERT INTO products (id, name, price) VALUES (2, 'timotey', 12.34);
INSERT INTO products (id, name, price) VALUES (3, 'pantine', 2.54);
INSERT INTO products (id, name, price) VALUES (4, 'schauma', 5.45);

INSERT INTO categories_products (id, category_id, product_id) VALUES (1, 1, 1);
INSERT INTO categories_products (id, category_id, product_id) VALUES (2, 1, 2);

INSERT INTO categories_products (id, category_id, product_id) VALUES (3, 2, 1);
INSERT INTO categories_products (id, category_id, product_id) VALUES (4, 2, 3);
INSERT INTO categories_products (id, category_id, product_id) VALUES (5, 2, 4);

set foreign_key_checks=1;
