set foreign_key_checks=0;

drop table if exists categories_products cascade;
drop table if exists categories cascade;
drop table if exists products cascade;

CREATE TABLE IF NOT EXISTS categories
(
    id   BIGINT AUTO_INCREMENT UNIQUE,
    name VARCHAR(256),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS products
(
    id    BIGINT AUTO_INCREMENT UNIQUE,
    name  VARCHAR(256),
    price DOUBLE,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS categories_products
(
    id          BIGINT AUTO_INCREMENT UNIQUE,
    category_id BIGINT,
    product_id  BIGINT,
    PRIMARY KEY (id),
    FOREIGN KEY (category_id) REFERENCES CATEGORIES (id),
    FOREIGN KEY (product_id) REFERENCES PRODUCTS (id) ON DELETE CASCADE
);

set foreign_key_checks=1;