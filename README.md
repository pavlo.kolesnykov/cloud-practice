# cloud-practice

_Application is based on Java 15_ \
To run application staying in a project root folder run command:

- via gradle (**running on port 3000**)
    - `gradlew :bootRun` - for Windows OS
    - `./gradlew :bootRun` - for *nix OS
- via docker (**running on port 3000**)
    - `docker run -d -p 3000:3000 lemoh/cloud-practice-hub`
- via docker-compose tool (**running on port 3010**)
    - `docker-compose up -d`

swagger documentation is available on http://localhost:3000/swagger-ui/ \
health endpoint is configured to be reached on http://localhost:3000/actuator/health \
and returns a string "up" \
\
docker image could be built via gradle command `:buildDocker`