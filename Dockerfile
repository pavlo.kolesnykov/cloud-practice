FROM azul/zulu-openjdk-alpine:11-jre
WORKDIR /usr/src/cloud-practice
ARG UNPACKED_JAR
COPY ${UNPACKED_JAR}/BOOT-INF/lib ./app/lib
COPY ${UNPACKED_JAR}/META-INF ./app/META-INF
COPY ${UNPACKED_JAR}/BOOT-INF/classes ./app
ENTRYPOINT ["java", "-cp", "app:./app/lib/*", "edu.cloud.practice.CloudPracticeApplication"]